from django.urls import path, include

from core.views import *

urlpatterns = [
    path('', GoodLoginView.as_view(), name='home'),
    path('login/', GoodLoginView.as_view(), name='login'),

    path('items/', ItemListView.as_view(), name='item_list'),
    path('items/add/', ItemCreateView.as_view(), name='item_add'),
    path('items/<int:pk>', ItemDetailView.as_view(), name='item'),
    path('items/<int:pk>/sold/', SoldView.as_view(), name='item_sold'),
    path('items/<int:pk>/change-status/', ChangeStatusView.as_view(), name='item_change_status'),
]
