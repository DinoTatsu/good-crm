from django.contrib import admin
from .models import *


class ItemAdmin(admin.ModelAdmin):
    form = ItemForm


admin.site.register(Item, ItemAdmin)
