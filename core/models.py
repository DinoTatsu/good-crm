from django.core.validators import MaxValueValidator
from django.db import models
from django import forms
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _

STATUS_CHOICES = (
    (1, _("есть в наличии")),
    (2, _("ожидает поступления")),
    (3, _("нет в наличии"))
)


def set_right_status(item):
    if item.number > 0:
        item.status = 1
    else:
        item.status = 3
    return item


class Item(models.Model):
    class Meta:
        verbose_name = 'Товар'
        verbose_name_plural = 'Товары'
        ordering = ['id']
        permissions = (
            ("can_sold_items", "Продать товар"),
            ("can_change_status", "Поменять статус"),
        )

    name = models.CharField(max_length=50, null=True, verbose_name=_('Товар'))
    status = models.IntegerField(choices=STATUS_CHOICES, default=3, verbose_name=_('Статус'))
    number = models.PositiveIntegerField(default=0, verbose_name=_('Количество'))

    def __str__(self):
        return str(self.name)

    def get_absolute_url(self):
        return reverse('item', kwargs={'pk': self.pk})


class ItemForm(forms.ModelForm):
    class Meta:
        model = Item
        fields = '__all__'

    def save(self, commit=True):
        item = super(ItemForm, self).save(commit=False)
        item = set_right_status(item)
        if commit:
            item.save()
        return item


class SoldForm(forms.ModelForm):
    class Meta:
        model = Item
        fields = []

    number = forms.IntegerField(min_value=0, required=True)


class ChangeStatusForm(forms.ModelForm):
    class Meta:
        model = Item
        fields = ['status']
