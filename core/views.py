from django.contrib.auth.mixins import PermissionRequiredMixin
from django.contrib.auth.models import Permission, Group
from django.contrib.auth.views import LoginView
from django.urls import reverse, reverse_lazy
from django.views.generic import FormView, ListView, CreateView, DetailView, UpdateView

from .models import *


add_item_permission = Permission.objects.get(codename='add_item')
sold_permission = Permission.objects.get(name='Продать товар')
change_status_permission = Permission.objects.get(name='Поменять статус')

provider, created = Group.objects.get_or_create(name='поставщик')
employee_group, created = Group.objects.get_or_create(name='сотрудник склада')

# Сотрудник склада: может только пользоваться кнопкой “Продали”.
employee_group.permissions.set([sold_permission])
# Поставщик: может только добавлять товары и менять статус на “Ожидает поступления”,
# если у наименования статус “Нет в наличии”.
provider.permissions.set([change_status_permission, add_item_permission])

# Завсклад: может добавлять, удалять и редактировать товары. Может выставлять все статусы для наименований.
# Может выставлять количество товара на складе. Завсклад может создавать новых пользователей и выставлять им роли.


class GoodLoginView(LoginView):
    template_name = 'sign-in.html'

    def get_success_url(self):
        if self.request.user.is_superuser:
            return reverse('admin:index')
        return reverse('item_list')


class ItemListView(ListView):
    model = Item
    template_name = 'item-list.html'


class ItemDetailView(DetailView):
    model = Item
    template_name = 'item.html'


class ItemCreateView(PermissionRequiredMixin, CreateView):
    model = Item
    form_class = ItemForm
    template_name = 'create_item.html'
    success_url = reverse_lazy('item_list')
    permission_required = ('core.add_item',)


class SoldView(PermissionRequiredMixin, UpdateView):
    '''У каждого товара есть кнопка “Продали”, нажатие на которую вызывает форму,
    в которой можно указать количество проданного товара и нажать кнопку “Ок”.
    После нажатия на кнопку “Продали”, от количества товара на складе должно отниматься число,
    которое указали в окне “Продали”. Нельзя продать больше, чем есть на складе.'''
    model = Item
    form_class = SoldForm
    template_name = 'change_number.html'
    success_url = reverse_lazy('item_list')
    permission_required = ('core.can_sold_items',)

    def form_valid(self, form):
        response = super(SoldView, self).form_valid(form)
        self.object.number -= form.cleaned_data['number']
        set_right_status(self.object)
        self.object.save()
        return response


class ChangeStatusView(PermissionRequiredMixin, UpdateView):
    model = Item
    fields = []
    template_name = 'item-list.html'
    success_url = reverse_lazy('item_list')
    permission_required = ('core.can_change_status',)
    
    def form_valid(self, form):
        self.object.status = 2
        self.object.save()
        return super(ChangeStatusView, self).form_valid(form)
